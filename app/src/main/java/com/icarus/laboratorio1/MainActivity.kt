package com.icarus.laboratorio1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcesar.setOnClickListener {
            val edad=edtEdad.text.toString().toInt();
            var msg:String
            msg=if(edad>=18) "Usted es mayor de edad" else "Usted es menor de edad"
            tvResultado.text=msg
        }
    }


}